package com.leblondM.OnlineHotel;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OnlineHotelApplication {

	public static void main(String[] args) {
		SpringApplication.run(OnlineHotelApplication.class, args);
	}

}
