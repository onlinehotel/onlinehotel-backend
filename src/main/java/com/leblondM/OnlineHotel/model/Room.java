package com.leblondM.OnlineHotel.model;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.OneToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.common.aliasing.qual.Unique;


@Data
@Builder
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class Room {

    @Id
    @GeneratedValue
    @Unique
    @NonNull

    private int id;

    @NonNull
    private int numberRoom;

    @OneToOne
    private Hotel hotelID;

    @NonNull
    private boolean tv;

    @NonNull
    private boolean wc;

    @NonNull
    private boolean bathtub;

    @NonNull
    private boolean shower;

    @NonNull
    private boolean occupied;

    @NonNull
    private int peopleNumber;


//        picture


}
