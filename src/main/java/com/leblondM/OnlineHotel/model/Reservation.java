package com.leblondM.OnlineHotel.model;


import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.antlr.v4.runtime.misc.NotNull;

import java.util.Date;
import java.util.List;
import org.checkerframework.checker.nullness.qual.NonNull;


@Data
@Builder
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class Reservation {

    @Id
    @GeneratedValue
    @NonNull
    private int id;

    @NonNull
    private Date startDate;

    @NonNull
    private Date endDate;

    @OneToMany
    private List<Room> roomID;

    @OneToOne
    private Client clientID;

    @NonNull
    private int reservationPrice;


}
