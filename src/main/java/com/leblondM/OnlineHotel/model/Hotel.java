package com.leblondM.OnlineHotel.model;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import lombok.*;
import org.checkerframework.checker.nullness.qual.NonNull;


@Data
@Builder
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class Hotel {

    @Id
    @GeneratedValue
    private int id;

    @NonNull
    private String name;

    @NonNull
    private String address;

    @NonNull
    private int zipCode;

    @NonNull
    private String city;

    @NonNull
    private String description;

    @NonNull
    private boolean restaurant;

    @NonNull
    private boolean swimmingPool;

    @NonNull
    private boolean elevator;

//    @NonNull
//    private BusinessHours operationHours;


//        pictures => on server ? BLOB ?


}
