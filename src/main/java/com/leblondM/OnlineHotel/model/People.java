package com.leblondM.OnlineHotel.model;


import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.antlr.v4.runtime.misc.NotNull;
import org.checkerframework.checker.nullness.qual.NonNull;


@Data
@Builder
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class People {

    @Id
    @GeneratedValue
    @NonNull
    private int id;

    @NonNull
    private String userName;

    @NonNull
    private String password;

}
