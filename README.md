Technologies utilisées : 
    Spring boot java

Deux branches existent : 
    master, avec une CI et docker relativement fonctionnelle
    dev : la connexion avec une base de données locale (et avec fichiers sql pour réimporter les données) pour faciliter l'exécution du code


Démarrer le projet : 
    mvn spring-boot:run

Pas encore de tests : le CRUD (JPA Repository) est une interface officielle, aucune raison que basiquement elle ne foncitonne pas.
Il reste à implémenter les features basiques (read, update, delete, create) les différentes données
